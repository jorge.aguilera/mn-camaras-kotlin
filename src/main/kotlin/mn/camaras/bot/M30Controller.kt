package mn.camaras.bot

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.reactivex.Single

@Controller("/m30")
class M30Controller(val m30Service: M30Service) {

    @Get("/")
    @Produces(MediaType.IMAGE_GIF)
    fun index(): Single<ByteArray> {

        return m30Service.buildGif()

    }
}