package mn.camaras.bot

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client
import io.reactivex.Flowable
import io.reactivex.Single

@Client("http://informo.munimadrid.es/cameras")
interface M30ImageRepo{

    @Get("/Camara{id}.jpg")
    fun downloadImage( id : String) : Single<ByteArray>

}