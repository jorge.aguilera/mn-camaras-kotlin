package mn.camaras.bot

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("mn.camaras.bot")
                .mainClass(Application.javaClass)
                .start()
    }
}