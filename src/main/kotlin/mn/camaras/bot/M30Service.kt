package mn.camaras.bot

import com.puravida.gif.GifGenerator
import io.micronaut.context.annotation.Value
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Produces
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Singleton

@Singleton
class M30Service(val m30ImageRepo :M30ImageRepo) {

    val items = arrayListOf<String>(
            "06301","11304","08317","02308","09309","09310","13305","13301","08303","08318","08307","08306"
    )

    var cache = ByteArray(0)

    fun buildGif(): Single<ByteArray> {
        if( cache.size != 0){
            return Single.create{ emitter->
                emitter.onSuccess(cache)
            }
        }else {
            return Single.create { emitter ->
                val images = mutableListOf<ByteArray>()
                items.forEach {
                    images.add(m30ImageRepo.downloadImage(it).blockingGet())
                }
                val gifGenerator = GifGenerator()
                cache = gifGenerator.composeBytes(images)
                emitter.onSuccess(cache)
            }
        }
    }
}
