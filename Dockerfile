FROM java:8u111-jdk as build
COPY . /home/app/mn-camaras-bot
WORKDIR /home/app/mn-camaras-bot
RUN ./gradlew build

FROM oracle/graalvm-ce:1.0.0-rc13 as graalvm
COPY --from=build /home/app/mn-camaras-bot /home/app/mn-camaras-bot
WORKDIR /home/app/mn-camaras-bot
RUN native-image --no-server -cp build/libs/mn-camaras-bot-*.jar

FROM java:8u111-jdk
EXPOSE 8080
COPY --from=graalvm /home/app/mn-camaras-bot .
ENTRYPOINT ["./mn-camaras-bot"]
